package UIPathPackage

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type refreshToken struct {
	AccessToken      string `json:"access_token"`
	RefreshToken     string `json:"refresh_token"`
	IDToken          string `json:"id_token"`
	Scope            string `json:"scope"`
	ExpiresIn        int    `json:"expires_in"`
	TokenType        string `json:"token_type"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

func GetRefreshToken(auth_code, code_verifier string) (string, string) {
	message := map[string]interface{}{
		"grant_type":    "authorization_code",
		"code":          auth_code,
		"redirect_uri":  "https://account.uipath.com/mobile",
		"code_verifier": code_verifier,
		"client_id":     "5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN",
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Requesting Refresh Token")
	resp, err := http.Post("https://account.uipath.com/oauth/token", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
		return "Error", ""
	}

	var result map[string]interface{}
	var response refreshToken

	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal([]byte(jsonDataFromHttp), &response)
	json.NewDecoder(resp.Body).Decode(&result)
	log.Println(response)
	if response.Error == "" {
		return "Success", string(response.RefreshToken)
	}
	//log.Println(response.Error)
	return "Error", response.ErrorDescription
}
