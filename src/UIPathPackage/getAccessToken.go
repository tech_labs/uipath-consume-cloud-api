package UIPathPackage

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type OauthToken struct {
	AccessToken      string `json:"access_token"`
	IDToken          string `json:"id_token"`
	Scope            string `json:"scope"`
	ExpiresIn        int    `json:"expires_in"`
	TokenType        string `json:"token_type"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

func GetAccessToken(refresh_token string) (string, string) {
	message := map[string]interface{}{
		"grant_type":    "refresh_token",
		"client_id":     "5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN",
		"refresh_token": refresh_token,
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Requesting Token")
	resp, err := http.Post("https://account.uipath.com/oauth/token", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
		return "Error", ""
	}

	var result map[string]interface{}
	var response OauthToken

	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal([]byte(jsonDataFromHttp), &response)
	json.NewDecoder(resp.Body).Decode(&result)
	if response.Error == "" {
		return "Success", string(response.AccessToken)
	}
	//log.Println(response.Error)
	return "Error", response.ErrorDescription
}
